﻿-- <Migration ID="1ebadda7-cfed-4555-a824-a949d6eb7e47" />
GO

PRINT N'Creating [dbo].[EMP]'
GO
CREATE TABLE [dbo].[EMP]
(
[empno] [int] NOT NULL,
[ename] [varchar] (10) NULL,
[job] [varchar] (9) NULL,
[mgr] [int] NULL,
[hiredate] [datetime] NULL,
[sal] [numeric] (7, 2) NULL,
[comm] [numeric] (7, 2) NULL,
[dept] [int] NULL
)
GO
PRINT N'Creating primary key [PK__EMP__AF4C318A965A8C49] on [dbo].[EMP]'
GO
ALTER TABLE [dbo].[EMP] ADD CONSTRAINT [PK__EMP__AF4C318A965A8C49] PRIMARY KEY CLUSTERED ([empno])
GO
